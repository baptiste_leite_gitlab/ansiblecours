# ansiblecours
Etape 1 :
Modifier le dockerfile:
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y net-tools
RUN apt-get install -y ssh

RUN echo 'root:root' |chpasswd

RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN service ssh start
EXPOSE 22
EXPOSE 80

ENTRYPOINT service ssh restart && bash

sudo docker build -t ubuntu1804_web .
sudo docker run -dti --name=host_web ubuntu1804_web

Modifier le dockerfile
FROM ubuntu:18.04

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y net-tools
RUN apt-get install -y ssh

RUN echo 'root:root' |chpasswd

RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN service ssh start
EXPOSE 22
EXPOSE 3306

ENTRYPOINT service ssh restart && bash

sudo docker build -t ubuntu1804_db .
sudo docker run -dti --name=host_db ubuntu1804_db
ssh-copy-id -i /home/azureuser/.ssh/id_rsa.pub root@172.17.0.4 => password du compte root: root
ssh root@172.17.0.4
ssh-copy-id -i /home/azureuser/.ssh/id_rsa.pub root@172.17.0.5 => password du compte root: root
ssh root@172.17.0.5
sudo vim /etc/ansible/hosts ajouter à la fin du fichier:
[host_web]
172.17.0.4
[host_db]
172.17.0.5

s'assurer que les hotes répondent avec la commande
ansible all -l 'host_web' -m ping -u root
ansible all -l 'host_db' -m ping -u root


avant de mettre en place MYSQL, nous allons télécharger le roles mysql défini par geerlingguy via ansible Galaxy:
ansible-galaxy install geerlingguy.mysql

